<?php

declare(strict_types=1);

namespace Evgeny\CodeBattle;

use Evgeny\CodeBattle\Entities\Opponent;

class CodeBattle
{
    /**
     * Участники соревнования
     * @var Opponent[]
     */
    public array $opponents;

    /**
     * Данные для обработки участниками
     * @var mixed
     */
    public mixed $competition;

    /**
     * Кол-во циклов выполнения скрипта
     * @var int
     */
    public int $rounds = 1;

    /**
     * Победитель
     * @var Opponent|null
     */
    public ?Opponent $winner = null;

    /**
     * Статистика
     * @var array
     */
    public array $statistics = [];

    public function addOpponent(Opponent $opponent): self
    {
        $this->opponents[] = $opponent;

        return $this;
    }

    public function createOpponent(string $name, \Closure $function): self
    {
        $this->opponents[] = Opponent::create()
            ->setName($name)
            ->setHit($function);

        return $this;
    }

    public function setRounds(int $count)
    {
        $this->rounds = $count;

        return $this;
    }


    public function setCompetition(array $array): self
    {
        $this->competition = $array;

        return $this;
    }

    /**
     * Главный метод
     *
     * @return void
     */
    public function fight(): void
    {
        foreach ($this->opponents as $opponent) {
            for ($i = 0; $i < $this->rounds; $i++) {
                $this->playRound($opponent);
            }
        }

        $this->calculateResults();
        $this->printResults();
    }

    private function playRound(Opponent $opponent): void
    {
        $start = microtime(true);

        $opponent->hit()($this->competition);

        $opponent->addResult(microtime(true) - $start);
    }

    private function calculateResults(): void
    {
        $bestResultOfAll = null;

        foreach ($this->opponents as $opponent) {
            $bestResult = StatisticsService::getBestResult($opponent);
            $averageResult = StatisticsService::getAverageResult($opponent);

            $this->statistics[] = [
                'name' => $opponent->getName(),
                'best result' => $bestResult,
                'average result' => $averageResult,
            ];

            if ($bestResultOfAll === null || $bestResult < $bestResultOfAll) {
                $bestResultOfAll = $bestResult;
                $this->winner = $opponent;
            }
        }
    }

    public static function create(): self
    {
        return new self();
    }

    private function printResults(): void
    {
        dump("----------------WINNER----------------");
        dump([
            'name' => $this->winner->getName(),
            'best_result' => StatisticsService::getBestResult($this->winner),
            'average result' => StatisticsService::getAverageResult($this->winner),
        ]);

        dump("----------------ALL_STATISTICS----------------");
        dump($this->statistics);
    }
}