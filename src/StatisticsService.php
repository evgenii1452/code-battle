<?php

declare(strict_types=1);

namespace Evgeny\CodeBattle;

use Evgeny\CodeBattle\Entities\Opponent;

class StatisticsService
{
    public static function getBestResult(Opponent $opponent): float
    {
        $bestResult = null;
        foreach ($opponent->getResults() as $result) {
            if ($bestResult === null || $result < $bestResult) {
                $bestResult = $result;
            }
        }

        return $bestResult;
    }

    public static function getAverageResult(Opponent $opponent): float
    {
        $total = 0;
        $count = 0;

        foreach ($opponent->getResults() as $result) {
            $total += $result;
            $count++;
        }

        return ($total / $count);
    }
}