<?php

declare(strict_types=1);

namespace Evgeny\CodeBattle\Entities;

class Opponent
{
    /**
     * Название
     *
     * @var string
     */
    private string $name;

    /**
     * Прием - анонимная функция,
     * которая будет выполняться
     *
     * @var \Closure
     */
    private \Closure $hit;

    /**
     * Результаты (время)
     *
     * @var array
     */
    private array $results = [];

    public static function create(): self
    {
        return new self();
    }

    public function setHit(\Closure $hit)
    {
        $this->hit = $hit;

        return $this;
    }

    public function hit(): \Closure
    {
        return $this->hit;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param float $result
     * @return $this
     */
    public function addResult(float $result): self
    {
        $this->results[] = $result;

        return $this;
    }
}