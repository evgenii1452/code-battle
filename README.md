Example:

```php
\Evgeny\CodeBattle\CodeBattle::create()
    ->setCompetition($array)
    ->createOpponent('isset && !== []', function (array $data) {
        return isset($data['test']) && $data['test'] !== [];
    })
    ->createOpponent('empty', function ($data) {
        return !empty($data['test']);
    })
    ->setRounds(5)
    ->fight();
```